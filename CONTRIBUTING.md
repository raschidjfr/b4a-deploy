# Contributing

## Build
```
$ make
```

## Test
This repository uses other dependencies as testing tools. In order to initialize them, after cloning the main repo, run:

```
$git submodule update --init
```

Then you can run the tests:

```
$ make test
```

## Run
1. create a `.env` file to pass environmental variables to the docker container:

    ```sh
    # .env

    # Required
    CLOUD_PATH=/absolute/path/to/cloud/folder       # folder containing the file `main.js`
    B4A_ACCOUNT_KEY=****
    B4A_APP_NAME=MyTestApp  # no whitespaces allowed here in .env file!

    # Optionals
    PUBLIC_PATH=/absolute/path/to/public/folder     # folder containing files to be uploaded to `public` folder
    DEBUG=1
    ```

2. `$ make run`
