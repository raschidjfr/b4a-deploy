#!/bin/sh
if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs) # Load vars from .env file
fi

docker run -ti \
-v "$PUBLIC_PATH":"$PUBLIC_PATH:ro" \
-v "$CLOUD_PATH":"$CLOUD_PATH:ro" \
-e CLOUD_PATH="$CLOUD_PATH" \
-e B4A_ACCOUNT_KEY="$B4A_ACCOUNT_KEY" \
-e B4A_APP_NAME="$B4A_APP_NAME" \
-e PUBLIC_PATH="$PUBLIC_PATH" \
-e DEBUG=$DEBUG \
-e MESSAGE="$MESSAGE" \
--name=b4a-deploy \
--rm \
raschidjfr/b4a-deploy
