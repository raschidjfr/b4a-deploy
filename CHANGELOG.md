# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.0.0

- major: feat!: abort if package.json is missing

## 0.2.1

- patch: Pipeline failed when selecting first app from the b4a list

## 0.2.0

- minor: Echo release description string on success

## 0.1.0

- minor: Support uploading projects and public files to Back4App
