FROM curlimages/curl:7.76.1
ARG B4A_CLI_VERSION=3.3.1
USER root
WORKDIR /root

RUN curl --compressed -Lo b4a https://github.com/back4app/parse-cli/releases/download/release_${B4A_CLI_VERSION}/b4a_linux; \
    chmod +x b4a; \
    mv b4a /usr/local/bin/b4a;

COPY entry.sh .
RUN chmod +x entry.sh
ENTRYPOINT /root/entry.sh
