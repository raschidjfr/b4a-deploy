.PHONY: test

build:
	docker build -t raschidjfr/b4a-deploy .

run:
	bin/run.sh

test:
	test/bats/bin/bats test/test.bats
