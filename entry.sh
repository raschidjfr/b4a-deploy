#!/bin/sh
set -e
echo "*** B4A DEPLOY ***";

# Set debug on if `DEBUG` is defined
if [ -z $DEBUG ]
then
  DEBUG=
else
  set -x
fi

CLOUD_PATH=${CLOUD_PATH:?'Var CLOUD_PATH is missing!'}
B4A_APP_NAME=${B4A_APP_NAME:?'Var B4A_APP_NAME is missing!'}
B4A_ACCOUNT_KEY=${B4A_ACCOUNT_KEY:?'Var B4A_ACCOUNT_KEY is missing!'}

# Check for package.json in source folder
FILE="${CLOUD_PATH}/package.json"
if [[ ! -f $FILE ]] ; then
    echo "File ${FILE} not found"
    exit
fi

# Run setup routine and copy files to a temp project folder
PROJECT_PATH=temp
echo "$B4A_ACCOUNT_KEY" | b4a configure accountkey
printf "e\n1\n$PROJECT_PATH\nb" | b4a new
cp -r "$CLOUD_PATH/." "$PROJECT_PATH/cloud"

if [ -z $PUBLIC_PATH ]
then
	:
else
	cp -r "$PUBLIC_PATH/." "$PROJECT_PATH/public"
fi

cd "$PROJECT_PATH"
CURRENT_APP=$(b4a default)

if [ "${CURRENT_APP:23}" == "$B4A_APP_NAME" ] # Is the app already selected?
then
  :
else
  b4a add "$B4A_APP_NAME"
  b4a default "$B4A_APP_NAME"
fi

MESSAGE=${MESSAGE:="b4a-deploy on $(date)"}
b4a deploy -d "$MESSAGE"
echo "done: $MESSAGE"
