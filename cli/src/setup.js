const {spawn, execSync} = require('child_process');
const {existsSync} = require('fs');
const path = require('path');
const rimraf = require('rimraf');
const mkdirp = require('mkdirp');

function installCLI() {
  console.log('Download CLI');
  execSync('curl https://raw.githubusercontent.com/back4app/parse-cli/back4app/installer.sh | /bin/sh');
}

/**
 * @param {string} accountKey
 * @return {Promise<void>}
 */
function setupAccountKey(accountKey) {
  return new Promise((resolve, reject) => {
    if (!accountKey) {
      return reject(new Error('Missing account key'));
    }

    console.log('Setup account key...\n');
    try {
      const child = spawn('b4a', ['configure', 'accountkey']);
      child.on('exit', (code) => code ? reject(code) : resolve(code));
      child.on('error', reject);
      child.on('message', console.log);
      child.stdout.pipe(process.stdout);
      child.stdout.on('data', () => {
        child.stdin.write(accountKey + '\n');
      });
    } catch (e) {
      return reject(e);
    }
  });
}

/**
 * @param {string} directory
 * @param {number} projectNumber
 * @return {Promise<number>}
 */
function setupDirectory(directory, projectNumber = 1) {
  projectNumber = (projectNumber || 1).toString();
  return new Promise((resolve, reject) => {
    console.log('Setup project...');

    try {
      console.log('> b4a new\n');
      const predefinedInputs = ['e', projectNumber, directory, 'b'];
      const child = spawn('b4a', ['new']);
      child.stdout.pipe(process.stdout);
      child.on('exit', (code) => code ? reject(code) : resolve(code));
      child.on('error', reject);
      child.on('message', console.log);
      child.stdout.on('data', (_data) => {
        const input = predefinedInputs.shift();
        child.stdin.write(input + '\n');
        process.stdout.write(input + '\n');
      });
    } catch (e) {
      return reject(e);
    }
  });
}
/**
 * @param {string} appName
 * @param {string} directory
 * @return {Promise<void>}
 */
function setDefaultApp(appName, directory) {
  return new Promise((resolve, reject) => {
    if (!appName) {
      return reject(new Error('Missing app name'));
    }

    try {
      const cwd = path.resolve(directory);
      let command = `b4a add "${appName}"`;
      console.log(`${cwd}> ${command}`);
      execSync(command, {cwd});

      command = `b4a default "${appName}"`;
      console.log(`${cwd}> ${command}`);
      execSync(command, {cwd});
      console.log('"%s" has been set as default app\n', appName);
      resolve(0);
    } catch (e) {
      if (e.message.includes('has already been added')) {
        return resolve(0);
      }
      return reject(e);
    }
  });
}

function deleteDir(directory) {
  return new Promise((resolve, reject) => {
    if (directory && existsSync(directory)) {
      rimraf(directory, (err) => {
        if (err) {
          return reject(err);
        }
        return resolve();
      });
    }
    return resolve();
  });
}

/**
 * @param {string} accountKey
 * @param {string} appName
 * @param {string} directory (relative executing location)
 * @param {Object} opts
 * @param {boolean} [opts.install=false] Download and install CLI before setup
 * @param {boolean} [opts.force=false] Delete target directory before setup
 * @return {Promise<void>}
 */
function setup(accountKey, appName, directory, opts = {}) {
  if (opts.install) {
    installCLI();
  }

  return setupAccountKey(accountKey)
    .then(async () => {
      if (opts.force) {
        await deleteDir(directory);
      }
      return setupDirectory(directory).catch((e) => {
        console.error('Failed to set up directory %s', directory);
        throw e;
      });
    })
    .then(() => {
      return setDefaultApp(appName, directory).catch((e) => {
        console.error('Failed to set default app "%s"', appName);
        throw e;
      });
    })
    .then(() => {
      const cloud = path.resolve(directory, 'cloud');
      const public = path.resolve(directory, 'public');
      return Promise.all([mkdirp(cloud), mkdirp(public)]).then(() => 0);
    })
    .then((code) => {
      if (code) throw new Error('Exit with error code ' + code);
      return 0;
    })
    .catch((e) => {
      console.error(e);
      process.exit(1);
    });
}

module.exports = setup;
