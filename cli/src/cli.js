const {program: cli} = require('commander');
const setup = require('./setup');
const deploy = require('./deploy');
const packageUtils = require('./packageUtils');

cli.version('0.1.0');

cli
  .command('setup <directory>')
  .description('setup a Back4App project')
  .requiredOption(
    '--key <account key>',
    'account key. Get one at https://bit.ly/3j6Xc0C')
  .requiredOption(
    '--app <app name>',
    'name of the app to link')
  .option(
    '-i, --install',
    'download and install CLI previous to setting up the project')
  .option(
    '-f, --force',
    'delete target directory before set up')
  .action((directory, cmdObj) => {
    setup(cmdObj.key, cmdObj.app, directory, {
      install: cmdObj.install,
      force: cmdObj.force,
    });
  });

cli
  .command('deploy <directory>')
  .description('deploy a directory to Back4App')
  .option(
    '-p, --public-dir <string>',
    'copy the content in this folder to public location on back4app')
  .option(
    '-f, --force',
    'deploy using flag -f')
  .action((directory, cmdObj) => {
    deploy(directory, {
      publicAssetDir: cmdObj.publicDir,
      force: cmdObj.force,
    });
  });

const packageCmd = cli
  .command('package <subcommand>')
  .description('tools for preprocessing package.json');

packageCmd
  .command('replace-module-aliases <path>')
  .description('Replace a string in all paths within the `_moduleAlias` key')
  .requiredOption(
    '-s <string>',
    'String to be replaced')
  .option(
    '-r <string>',
    'Replacement for the input string')
  .action((path, cmdObj) => {
    packageUtils.replaceModuleAliases(path, cmdObj.s, cmdObj.r || '');
  });

packageCmd
  .command('remove-dev-dependencies <path>')
  .description('Remove all dev dependencies in package.json. ' +
    'This is useful to avoid installing for example Parse Server or Expres on b4a')
  .action((path, cmdObj) => {
    packageUtils.removeDevDependencies(path);
  });

cli.parse(process.argv);
