const path = require('path');
const {existsSync} = require('fs');
const {spawn} = require('child_process');
const mkdirp = require('mkdirp');
const {ncp} = require('ncp');

/**
 * @param {string} dirName Directory containing cloud code files to upload
 * @param {boolean} force If `false`, only changed files will upload.
 * Default: `false`
 * @param {boolean} [debug=false]
 */
async function deployToBack4App(dirName, force = false, debug = false) {
  console.log('\nStarting deploy...\n');
  dirName = path.resolve(dirName);
  const cwd = path.resolve(dirName);
  const publicPath = path.join(cwd, 'public');

  // Create `public` directory if missing
  if (!existsSync(publicPath)) {
    await mkdirp(publicPath);
    console.log(`Created directory 'public': ${publicPath}`);
  }

  const command = 'b4a';
  const args = `deploy${force ? ' -f' : ''}`.split(' ');

  console.log(`${cwd}> ${command} ${args.join(' ')}\n`);
  const child = spawn(command, args, {cwd});

  if (debug) {
    child.stdout.pipe(process.stdout);
    child.stderr.pipe(process.stderr);
  }

  child.on('close', (code) => {
    if (code) {
      console.log('\x1b[31mCould not upload!\x1b[0m\n');
    } else {
      console.log(`\x1b[32mDone!\x1b[0m\n`);
    }
  });
}

/**
 * @param {string} sourceDir
 * @param {string} destDir
 * @return {Promise<void>}
 */
async function copyPublicDataContent(sourceDir, destDir) {
  console.log('\nCopy public folder %s...\n', sourceDir ? `"${sourceDir}"` : '');
  return new Promise(async (resolve, reject) => {
    if (!sourceDir || !existsSync(sourceDir)) {
      console.log('No public data to copy');
      return resolve();
    }

    const publicPath = `${destDir}/public`;
    if (!existsSync(publicPath)) {
      await mkdirp(publicPath);
    }

    console.log('Copy %s to %s', sourceDir, destDir);
    ncp(sourceDir, publicPath, function(err) {
      return err ? reject(err) : resolve();
    });
  });
}

/**
 *
 * @param {string} codePath Path to a B4A project directory. (Relative to project)
 * @param {Object} opts
 * @param {string} [opts.publicAssetDir=null] Path to directory
 * with the content of remote `public` folder
 * @param {boolean} [opts.force=false] Pass flag `-f` to b4a CLI
 */
async function deploy(codePath, opts = {}) {
  console.log('deploy cloud code folder "%s"', codePath);
  const folderCloud = path.resolve(codePath);
  if (!existsSync(path.join(folderCloud, 'main.js'))) {
    throw new Error(`main.js was not found in ${folderCloud}`);
  }

  await copyPublicDataContent(opts.publicAssetDir, codePath);
  await deployToBack4App(codePath, opts.force, true);
}

module.exports = deploy;
