const {readFileSync, writeFileSync} = require('fs');
const path = require('path');

/**
 * @param {string} srcPath Path to package.json file or its directory
 * @param {string} toReplace String to replace in paths within `_moduleAlias` key
 * @param {string} [replacement=''] The replacement for the string
 */
function replaceModuleAliases(srcPath, toReplace, replacement = '') {
  const filePath = path.join(srcPath.replace('package.json', ''), 'package.json');
  const text = readFileSync(filePath).toString();
  const pkg = JSON.parse(text);

  console.log('Search for _modulAliases in package.json...');

  const moduleAlias = pkg._moduleAliases;
  if (moduleAlias) {
    Object.keys(moduleAlias).forEach((k) => {
      const exp = new RegExp(`^${toReplace}\/?`);
      let aliasPath = moduleAlias[k].replace(exp, `${replacement}/`).replace(/\/+$/, '') || '.';
      aliasPath = aliasPath.indexOf('/') === 0 ? aliasPath.slice(1) : aliasPath;

      console.log('\t%s => %s', moduleAlias[k], aliasPath);
      moduleAlias[k] = aliasPath;
    });
  } else {
    console.warn('_moduleAliases not found in package.json');
  }

  writeFileSync(filePath, JSON.stringify(pkg, null, '\t'));
}

/**
 * @param {string} srcPath Path to package.json file or its directory
 */
function removeDevDependencies(srcPath) {
  const filePath = path.join(srcPath.replace('package.json', ''), 'package.json');
  const text = readFileSync(filePath).toString();
  const pkg = JSON.parse(text);

  console.log('Search and remove dev dependencies...');

  if (pkg.devDependencies) {
    delete pkg.devDependencies;
    writeFileSync(filePath, JSON.stringify(pkg, null, '\t'));
    console.log('Removed!');
  } else {
    console.log('No dev dependencies found in package.json');
  }
}

module.exports = {
  replaceModuleAliases,
  removeDevDependencies,
};
