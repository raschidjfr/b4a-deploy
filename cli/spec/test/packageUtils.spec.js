const fs = require('fs');
const path = require('path');
const {pathToTestProject} = require('../helpers');

describe('Package Utils', () => {
  const pathToCloudProject = path.resolve(pathToTestProject, 'dist');
  let packageUtils;

  function setOriginalPackageJson(json) {
    fs.writeFileSync(path.join(pathToCloudProject, 'package.json'), JSON.stringify(json));
  }

  beforeEach(() => {
    packageUtils = require('../../src/packageUtils');
  });

  it('removes parse-server and express', () => {
    setOriginalPackageJson({
      dependencies: {
        'express': '^4.17.1',
        'parse-server': '^4.3.0',
      },
      devDependencies: {
        'express': '^4.17.1',
        'parse-server': '^4.3.0',
      },
    });
    packageUtils.removeDevDependencies(pathToCloudProject);

    const pkgCopy = JSON.parse(fs.readFileSync(path.join(pathToCloudProject, 'package.json')).toString());
    expect(pkgCopy.devDependencies).toBeUndefined();
  });

  it('replace-module-aliases', () => {
    setOriginalPackageJson({
      _moduleAliases: {
        '@app': 'dist/cloud',
        '@env': 'dist/cloud/env',
        '@modules': 'dist/cloud/modules',
      },
    });
    packageUtils.replaceModuleAliases(pathToCloudProject, 'dist/cloud', 'foo');

    const pkgCopy = JSON.parse(fs.readFileSync(path.join(pathToCloudProject, 'package.json')).toString());
    expect(pkgCopy._moduleAliases['@app']).toBe('foo');
    expect(pkgCopy._moduleAliases['@env']).toBe('foo/env');
    expect(pkgCopy._moduleAliases['@modules']).toBe('foo/modules');
  });
});
