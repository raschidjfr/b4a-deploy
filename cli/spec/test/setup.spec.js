/* eslint-disable camelcase */
const proxyquire = require('proxyquire');
const rewire = require('rewire');
const {MockSpawn} = require('../helpers/mocks');
const rimraf = require('rimraf');

describe('Setup', () => {
  let child_process;
  let spawnMock;

  function removeTmpDir(path) {
    return new Promise((resolve, reject) => {
      rimraf(path, (err) => {
        if (err) reject(err);
        else resolve();
      });
    });
  }

  beforeEach(() => {
    spawnMock = new MockSpawn();
    child_process = {
      spawn: () => spawnMock,
      execSync: jasmine.createSpy('execSync'),
    };
  });

  afterAll(async () => {
    await removeTmpDir('temp');
  });

  it('can install', () => {
    const setup = proxyquire('../../src/setup', {child_process});
    setup('accountKey', 'appName', 'directory', {install: true});
    expect(child_process.execSync)
      .toHaveBeenCalledWith('curl https://raw.githubusercontent.com/back4app/parse-cli/back4app/installer.sh | /bin/sh');
  });

  it('can set up account key', () => {
    const b4aOutput = `
    Input your account key or press ENTER to generate a new one.
    NOTE: on pressing ENTER we'll try to open the url:
            "http://dashboard.back4app.com/classic#/wizard/account-key"
    in default browser.
    Account Key: `;

    const setup = proxyquire('../../src/setup', {child_process});
    setup('accountKey');
    spawnMock._callbacks.stdout.data(b4aOutput);
    expect(spawnMock.stdin.write).toHaveBeenCalledWith('accountKey\n');
  });

  it('can create directory', (done) => {
    const setup = rewire('../../src/setup');

    const testRoutine = () => {
      const setupDirectory = setup.__get__('setupDirectory');
      const outputs = [
        `Would you like to create a new app, or add Cloud Code to an existing app?
      Type "(n)ew" or "(e)xisting": `,

        `1:      A Test App
      Select an App to add to config: `,

        `Please enter the name of the folder where we can download the latest deployed
      Cloud Code for your app "[App Name Here]"

      Directory Name: `,

        `You can either set up a blank project or download the current deployed Cloud Code
      Please type "(b)lank" if you wish to setup a blank project, otherwise press ENTER: `,
      ];

      setupDirectory('path/to/project/folder').then(done);

      spawnMock._callbacks.stdout.data(outputs.shift());
      expect(spawnMock.stdin.write).toHaveBeenCalledWith('e\n');
      spawnMock._callbacks.stdout.data(outputs.shift());
      expect(spawnMock.stdin.write).toHaveBeenCalledWith('1\n');
      spawnMock._callbacks.stdout.data(outputs.shift());
      expect(spawnMock.stdin.write).toHaveBeenCalledWith('path/to/project/folder\n');
      spawnMock._callbacks.stdout.data(outputs.shift());
      expect(spawnMock.stdin.write).toHaveBeenCalledWith('b\n');
      spawnMock._callbacks.exit(0);
    };

    const runModule = setup.__with__({spawn: child_process.spawn});
    runModule(testRoutine);
  });

  it('can force', (done) => {
    const setup = rewire('../../src/setup');
    const rimraf = jasmine.createSpy('rimraf');

    const testRoutine = () => {
      setup.__set__('existsSync', () => true);
      setup.__set__('rimraf', rimraf);
      setup.__set__('setDefaultApp', () => Promise.resolve());
      setup.__set__('setupDirectory', async () => {
        expect(rimraf.calls.mostRecent().args[0]).toBe('temp/some/directory');
        done();
      });
      setup(null, null, 'temp/some/directory', {force: true});
    };

    const runModule = setup.__with__({
      setupAccountKey: () => Promise.resolve(),
    });

    runModule(testRoutine);
  });

  it('can set default app', (done) => {
    const {join} = require('path');
    const setup = rewire('../../src/setup');

    const testRoutine = () => {
      const cwd = join(__dirname, '../..', 'path/to/project');
      setup.__set__('__dirname', __dirname);
      setup.__set__('execSync', child_process.execSync);

      const setDefaultApp = setup.__get__('setDefaultApp');
      setDefaultApp('MyApp', 'path/to/project').then(done);

      expect(child_process.execSync.calls.argsFor(0)).toEqual(['b4a add "MyApp"', {cwd}]);
      expect(child_process.execSync.calls.argsFor(1)).toEqual(['b4a default "MyApp"', {cwd}]);
      done();
    };

    const runModule = setup.__with__({child_process});
    runModule(testRoutine);
  });
});
