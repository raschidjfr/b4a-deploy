const proxyquire = require('proxyquire');

describe('CLI', () => {
  let stubs = {};

  beforeAll(() => {
    stubs = {
      './setup': jasmine.createSpy('setup'),
      './deploy': jasmine.createSpy('deploy'),
      './packageUtils': {
        removeDevDependencies: jasmine.createSpy('removeDevDependencies'),
        replaceModuleAliases: jasmine.createSpy('replaceModuleAliases'),
      },
    };
  });

  it('setup', () => {
    process.argv = 'node cli.js setup --key=myAccountKey --app=myAppName --install --force tmp/some/dir'.split(' ');
    proxyquire('../../src/cli', stubs);
    expect(stubs['./setup']).toHaveBeenCalledWith('myAccountKey', 'myAppName', 'tmp/some/dir', {install: true, force: true});
  });

  it('deploy', () => {
    process.argv = 'node cli.js deploy -p path/to/assets -f tmp/some/dir'.split(' ');
    proxyquire('../../src/cli', stubs);
    expect(stubs['./deploy']).toHaveBeenCalledWith('tmp/some/dir', {
      publicAssetDir: 'path/to/assets',
      force: true,
    });
  });

  describe('package', () => {
    it('replace-module-aliases', () => {
      process.argv = [
        'node',
        'cli.js',
        'package',
        'replace-module-aliases',
        '-s',
        'replace me',
        '-r',
        'with this',
        'path/to/package.json',
      ];
      proxyquire('../../src/cli', stubs);
      expect(stubs['./packageUtils'].replaceModuleAliases)
        .toHaveBeenCalledWith('path/to/package.json', 'replace me', 'with this');
    });

    it('remove-dev-dependencies', () => {
      process.argv = [
        'node',
        'cli.js',
        'package',
        'remove-dev-dependencies',
        'path/to/package.json',
      ];
      proxyquire('../../src/cli', stubs);
      expect(stubs['./packageUtils'].removeDevDependencies)
        .toHaveBeenCalledWith('path/to/package.json');
    });
  });
});
