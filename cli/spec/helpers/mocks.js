class MockSpawn {
  _callbacks = {
    exit: null,
    error: null,
    message: null,
    stdout: {
      data: null
    }
  };

  stdout = {
    pipe: () => {},
    on: (event, cb) => this._callbacks.stdout[event] = cb,
  }

  stdin = {
    write: jasmine.createSpy('write')
  }

  on(event, cb) {
    this._callbacks[event] = cb;
  }
}

module.exports = {
  MockSpawn
};
